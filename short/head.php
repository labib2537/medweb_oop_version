<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>MedWeb - Dashboard</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="../global_assets/js/main/jquery.min.js"></script>
	<script src="../global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="../global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="../global_assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="../global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="../global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="../global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="../global_assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="../global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="../global_assets/js/demo_pages/form_layouts.js"></script>

	<script src="../global_assets/js/plugins/uploaders/plupload/plupload.full.min.js"></script>
	<script src="../global_assets/js/plugins/uploaders/plupload/plupload.queue.min.js"></script>
	<script src="../global_assets/js/demo_pages/uploader_plupload.js"></script>

	<script src="../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script src="../global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="../global_assets/js/demo_pages/datatables_basic.js"></script>

	<script src="../assets/js/app.js"></script>
	<script src="../global_assets/js/demo_pages/dashboard.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/streamgraph.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/sparklines.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/lines.js"></script>	
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/areas.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/donuts.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/bars.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/progress.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/heatmaps.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/pies.js"></script>
	<script src="../global_assets/js/demo_charts/pages/dashboard/light/bullets.js"></script>
	<!-- /theme JS files -->

	<script src="../global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="../global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="../global_assets/js/plugins/pickers/anytime.min.js"></script>
	<script src="../global_assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script src="../global_assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script src="../global_assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script src="../global_assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script src="../global_assets/js/plugins/notifications/jgrowl.min.js"></script>

    <script src="../global_assets/js/demo_pages/picker_date.js"></script>



	<!-- Theme JS files -->
	<script src="../global_assets/js/plugins/buttons/spin.min.js"></script>
	<script src="../global_assets/js/plugins/buttons/ladda.min.js"></script>

	<script src="../global_assets/js/demo_pages/components_buttons.js"></script>
	<!-- /theme JS files -->

<!-- Theme JS files -->
<script src="../global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>

<script src="../global_assets/js/demo_charts/echarts/light/bars/columns_basic.js"></script>
<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script src="../global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="../global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>

	<script src="../global_assets/js/demo_pages/widgets_stats.js"></script>
	<!-- /theme JS files -->

	<script src="../global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="../global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script src="../global_assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script src="../global_assets/js/demo_pages/uploader_bootstrap.js"></script>

</head>